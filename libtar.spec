Name:           libtar
Version:        1.2.20
Release:        25
Summary:        Library for manipulating tar files from within C programs.
License:        BSD
URL:            http://repo.or.cz/libtar.git
Source:         http://repo.or.cz/libtar.git/snapshot/refs/tags/v1.2.20.tar.gz#/libtar-v1.2.20.tar.gz

Patch0:         libtar-1.2.11-missing-protos.patch
Patch1:         libtar-1.2.11-mem-deref.patch
Patch2:         libtar-1.2.20-fix-resource-leaks.patch
Patch3:         libtar-1.2.11-bz729009.patch
Patch4:         libtar-1.2.20-no-static-buffer.patch
Patch5:         CVE-2013-4420.patch

Patch9000:      openEuler-CVE-2021-33643-CVE-2021-33644.patch 
Patch9001:      openEuler-CVE-2021-33645-CVE-2021-33646.patch
Patch9002:      CVE-2021-33640-fix-memory-leak-and-use-after-free-bugs.patch

BuildRequires:  libtool 

%description
Libtar is a C library for manipulating POSIX tar files. It handles adding
and extracting files to/from a tar archive. Requires gcc, make, and zlib.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and header files of %{name} are all in the %{name}-devel package.

%package	help
Summary:	Help manual for %{name}

%description    help
The %{name}-help package conatins man manual etc

%prep
%autosetup -n %{name}-v%{version} -p1

# set correct version for .so build
%global ltversion %(echo %{version} | tr '.' ':')
sed -i 's/-rpath $(libdir)/-rpath $(libdir) -version-number %{ltversion}/' \
  lib/Makefile.in

autoreconf --force --install

%build
%configure --disable-static
%make_build

%install
%make_install
# Without this we get no debuginfo and stripping
chmod +x $RPM_BUILD_ROOT%{_libdir}/libtar.so.%{version}
rm $RPM_BUILD_ROOT%{_libdir}/*.la


%ldconfig_scriptlets

%files
%doc TODO README ChangeLog*
%license COPYRIGHT
%{_bindir}/%{name}
%{_libdir}/lib*.so.*

%files devel
%{_includedir}/libtar.h
%{_includedir}/libtar_listhash.h
%{_libdir}/lib*.so

%files help
%{_mandir}/man3/*.3*

%changelog
* Thu Nov 24 2022 shixuantong <shixuantong1@huawei.com> - 1.2.20-25
- fix memory leak and use-after-free bugs of struct TAR *t

* Fri Jul 29 2022 shixuantong <shixuantong@h-partners.com> - 1.2.20-24
- fix CVE-2021-33643 CVE-2021-33644 CVE-2021-33645 CVE-2021-33646

* Sat May 07 2022 shixuantong <shixuantong@h-partners.com> - 1.2.20-23
- fix memory leak

* Sat May 07 2022 shixuantong <shixuantong@h-partners.com> - 1.2.20-22
- fix sz < 0

* Wed Apr 06 2022 shixuantong <shixuantong@h-partners.com> - 1.2.20-21
- Ensure that sz is greater than 0.

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.20-20
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Tue Jul 27 2021 yuanxin <yuanxin24@huawei.com> - 1.2.20-19
- remove BuildRequires gdb

* Mon Sep 14 2020 shixuantong <shixuantong@huawei.com> - 1.2.20-18
- fix CVE-2013-4420

* Wed Dec 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.20-17
- update license info

* Fri Oct 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.20-16
- Package init
